package eu.catalyst.mcm.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import eu.catalyst.mcm.payload.MarketAction;
import eu.catalyst.mcm.payload.MarketActionCounterOffer;
import eu.catalyst.mcm.payload.MarketSessionPlain;
import eu.catalyst.mcm.payload.PayloadMEMO2MCMNotFlex;
import eu.catalyst.mcm.payload.PrioritisedAction;


@Stateless
@Path("/postClearingProcess/")
public class PostClearingProcess {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ClearingProcess.class);

	public PostClearingProcess() {
	}

	@POST
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public PayloadMEMO2MCMNotFlex postClearingProcess(PayloadMEMO2MCMNotFlex inputPayloadMEMO2MCMNotFlex) {
		log.debug("-> postClearingProcess");

		PayloadMEMO2MCMNotFlex returnPayloadMEMO2MCMNotFlex = new PayloadMEMO2MCMNotFlex();
		returnPayloadMEMO2MCMNotFlex
				.setInformationBrokerServerUrl(inputPayloadMEMO2MCMNotFlex.getInformationBrokerServerUrl());
		returnPayloadMEMO2MCMNotFlex.setTokenMEMO(inputPayloadMEMO2MCMNotFlex.getTokenMEMO());
		returnPayloadMEMO2MCMNotFlex.setTokenMCM(inputPayloadMEMO2MCMNotFlex.getTokenMCM());
		returnPayloadMEMO2MCMNotFlex
				.setMarketClearingManagerUrl(inputPayloadMEMO2MCMNotFlex.getMarketClearingManagerUrl());
		returnPayloadMEMO2MCMNotFlex.setInformationBrokerId(inputPayloadMEMO2MCMNotFlex.getInformationBrokerId());

		ArrayList<MarketActionCounterOffer> returnMarketActionCounterOfferList = new ArrayList<MarketActionCounterOffer>();

		// --------------------------------------------------------------------------
		try {
			if (!inputPayloadMEMO2MCMNotFlex.getPrioritisedActionsList().isEmpty()) {

				for (Iterator<PrioritisedAction> iterator = inputPayloadMEMO2MCMNotFlex.getPrioritisedActionsList()
						.iterator(); iterator.hasNext();) {

					PrioritisedAction myPrioritisedAction = iterator.next();

					List<MarketAction> marketActionList = new ArrayList<MarketAction>();

					marketActionList.add(myPrioritisedAction.getMarketaction());

					URL url = new URL(
							inputPayloadMEMO2MCMNotFlex.getInformationBrokerServerUrl() + "/marketplace/"
									+ inputPayloadMEMO2MCMNotFlex.getMarketSession().getMarketplaceid().getId()
											.intValue()
									+ "/marketsessions/"
									+ inputPayloadMEMO2MCMNotFlex.getMarketSession().getId().intValue() + "/clearing/actions/");

					log.debug("Start - Rest url: " + url.toString());
					HttpURLConnection connService = (HttpURLConnection) url.openConnection();
					connService.setDoOutput(true);
					connService.setRequestMethod("POST");
					connService.setRequestProperty("Content-Type", "application/json");
					connService.setRequestProperty("Accept", "application/json");
					connService.setRequestProperty("Authorization", inputPayloadMEMO2MCMNotFlex.getTokenMCM());

					GsonBuilder gb = new GsonBuilder();
					gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
					Gson gson = gb.create();
					String record = gson.toJson(marketActionList);
					log.debug("Rest Request: " + record);

					OutputStream os = connService.getOutputStream();
					os.write(record.getBytes());

					int myResponseCode = connService.getResponseCode();
					log.debug("HTTP error code :" + myResponseCode);

					os.flush();
					os.close();
					connService.disconnect();

				}
			}

			// Invoke Rest Service (POST) "/marketplace/{marketId}/counteroffers" of IB
			// passing marketActionCounterOfferList

			if (!inputPayloadMEMO2MCMNotFlex.getMarketActionCounterOfferList().isEmpty()) {

				URL url = new URL(inputPayloadMEMO2MCMNotFlex.getInformationBrokerServerUrl() + "/marketplace/"
						+ inputPayloadMEMO2MCMNotFlex.getMarketSession().getMarketplaceid().getId()
						+ "/counteroffers/");

				log.debug("Rest url: " + url.toString());

				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
				connService.setDoOutput(true);
				connService.setRequestMethod("POST");
				connService.setRequestProperty("Accept", "application/json");
				connService.setRequestProperty("Content-Type", "application/json");

				connService.setRequestProperty("Authorization", inputPayloadMEMO2MCMNotFlex.getTokenMCM());
				GsonBuilder gb = new GsonBuilder();

				gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson = gb.create();

				String record = gson.toJson(inputPayloadMEMO2MCMNotFlex.getMarketActionCounterOfferList());

				System.out.println("Rest Request: " + record);

				OutputStream os = connService.getOutputStream();
				os.write(record.getBytes());

				int myResponseCode = connService.getResponseCode();
				log.debug("HTTP error code :" + myResponseCode);

				BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
				Type listType = new TypeToken<ArrayList<MarketActionCounterOffer>>() {
				}.getType();
				gb = new GsonBuilder();
				gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				gson = gb.create();
				returnMarketActionCounterOfferList = gson.fromJson(br, listType);
				log.debug("payload received: " + gson.toJson(returnMarketActionCounterOfferList));
				os.flush();
				os.close();
				br.close();
				connService.disconnect();

			}

			// Invoke Rest Service (PUT) "marketplace/{marketId}/marketsessions" of IB
			// passing market.getMarketSessionid() wrapped as ARRAYLIST
			// getting the first MarketSession of bidsOffersActionList

			// MarketSession myMarketSession = null;
			if ((inputPayloadMEMO2MCMNotFlex.getClearingPrice() != null)
					&& !inputPayloadMEMO2MCMNotFlex.getPrioritisedActionsList().isEmpty()) {
				inputPayloadMEMO2MCMNotFlex.getMarketSession()
						.setClearingPrice(inputPayloadMEMO2MCMNotFlex.getClearingPrice());

				URL url = new URL(inputPayloadMEMO2MCMNotFlex.getInformationBrokerServerUrl() + "/marketplace/"
						+ inputPayloadMEMO2MCMNotFlex.getMarketSession().getMarketplaceid().getId() + "/marketsessions/"
						+ inputPayloadMEMO2MCMNotFlex.getMarketSession().getId().intValue() + "/");

				log.debug("Rest url: " + url.toString());

				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
				connService.setDoOutput(true);
				connService.setRequestMethod("PUT");
				connService.setRequestProperty("Accept", "application/json");
				connService.setRequestProperty("Content-Type", "application/json");
				connService.setRequestProperty("Authorization", inputPayloadMEMO2MCMNotFlex.getTokenMCM());
				GsonBuilder gb = new GsonBuilder();

				// gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
				gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson = gb.create();

				// setting MarketSession only Ids
				MarketSessionPlain myMarketSessionPlain = new MarketSessionPlain();
				myMarketSessionPlain.setId(inputPayloadMEMO2MCMNotFlex.getMarketSession().getId());
				myMarketSessionPlain
						.setSessionStartTime(inputPayloadMEMO2MCMNotFlex.getMarketSession().getSessionStartTime());
				myMarketSessionPlain
						.setSessionEndTime(inputPayloadMEMO2MCMNotFlex.getMarketSession().getSessionEndTime());
				myMarketSessionPlain
						.setDeliveryStartTime(inputPayloadMEMO2MCMNotFlex.getMarketSession().getDeliveryStartTime());
				myMarketSessionPlain
						.setDeliveryEndTime(inputPayloadMEMO2MCMNotFlex.getMarketSession().getDeliveryEndTime());
				myMarketSessionPlain.setSessionStatusid(
						inputPayloadMEMO2MCMNotFlex.getMarketSession().getSessionStatusid().getId());
				myMarketSessionPlain
						.setMarketplaceid(inputPayloadMEMO2MCMNotFlex.getMarketSession().getMarketplaceid().getId());
				myMarketSessionPlain.setFormid(inputPayloadMEMO2MCMNotFlex.getMarketSession().getFormid().getId());
				myMarketSessionPlain
						.setClearingPrice(inputPayloadMEMO2MCMNotFlex.getMarketSession().getClearingPrice());

				// String record = gson.toJson(inputPayloadMEMO2MCMNotFlex.getMarketSession());
				String record = gson.toJson(myMarketSessionPlain);

				log.debug("Rest Request: " + record);

				OutputStream os = connService.getOutputStream();
				os.write(record.getBytes());

				int myResponseCode = connService.getResponseCode();
				log.debug("HTTP error code :" + myResponseCode);
				os.flush();
				os.close();
				connService.disconnect();

			}

		} catch (

		Exception e) {
			e.printStackTrace();
		}

		// --------------------------------------------------------------------------
		returnPayloadMEMO2MCMNotFlex.setClearingPrice(inputPayloadMEMO2MCMNotFlex.getClearingPrice());
		returnPayloadMEMO2MCMNotFlex.setMarketActionCounterOfferList(returnMarketActionCounterOfferList);
		returnPayloadMEMO2MCMNotFlex.setPrioritisedActionsList(inputPayloadMEMO2MCMNotFlex.getPrioritisedActionsList());
		returnPayloadMEMO2MCMNotFlex.setMarketSession(inputPayloadMEMO2MCMNotFlex.getMarketSession());

		log.debug("<- postClearingProcess");
		return returnPayloadMEMO2MCMNotFlex;
	}

}
